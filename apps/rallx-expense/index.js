import 'reflect-metadata';
import 'react-native-get-random-values';
import { registerRootComponent } from 'expo';

import { ExpoRoot } from 'expo-router';

export function App() {
  const ctx = require.context('./src/app');
  return <ExpoRoot context={ctx} />;
}

registerRootComponent(App);
