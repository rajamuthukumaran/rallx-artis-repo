import { StyleSheet } from 'react-native'

export const layout = StyleSheet.create({
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerCol: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  asideRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  asideCol: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  lrPartition: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  tdPartition: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  floatRight: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  alignRight: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
})
