import { NumChar } from '@rallx-artis/common-types';

// -----------------------------------------------------------------------
export type CurrencyProps = (
  cur: number | string,
  config?: {
    type?: 'INR' | 'USD' | string;
    zeroOnNull?: boolean;
  }
) => string | null;
export const currency: CurrencyProps = (cur, config) => {
  const { type = 'INR', zeroOnNull = true } = config || {};
  const curType = type !== 'INR' ? 'en-US' : 'en-IN';

  if (cur || cur === 0) {
    const curConv = convertToInt(cur);
    return Intl.NumberFormat(curType, {
      style: 'currency',
      currency: type,
    }).format(curConv);
  }

  if (zeroOnNull) {
    return Intl.NumberFormat(curType, {
      style: 'currency',
      currency: type,
    }).format(0);
  }

  return null;
};

// -----------------------------------------------------------------------

export type NumberFmtProps = (
  cur: number | string,
  config?: {
    type?: 'IN' | 'US' | string;
    zeroOnNull?: boolean;
    activeChange?: boolean; // enable if it is a active change like textfield
  }
) => string | null;

export const numberFmt: NumberFmtProps = (num, config) => {
  const { type = 'IN', zeroOnNull, activeChange } = config || {};
  const fmtType = type !== 'IN' ? 'en-US' : 'en-IN';

  const intNum = convertToInt(num);

  if (intNum) {
    const converted = intNum.toLocaleString(fmtType);

    if (activeChange && `${num}`.charAt((`${num}`?.length || 1) - 1) === '.') {
      return `${converted}.`;
    }

    return converted;
  }

  return zeroOnNull ? '0' : '';
};

// -----------------------------------------------------------------------

export const convertToInt = (value?: NumChar) => {
  if (typeof value === 'string') return parseFloat(value);
  if (typeof value === 'number') return value;
  return 0;
};

// -----------------------------------------------------------------------

export const prefixZeroToInt = (value = 0, base = 10) => {
  const zeros = base / 10;
  let prefix = '';

  for (let i = 0; i < zeros; i++) {
    prefix += '0';
  }

  return value < base ? `${prefix}${value}` : value;
};
