import { AccountTypes } from '../constants/account'

export type AccountData = {
  name: string
  account_type: AccountTypes
  balance?: number
  min_balance?: number
  exp_date?: Date
  description?: string
  include_in_bal?: boolean
}
