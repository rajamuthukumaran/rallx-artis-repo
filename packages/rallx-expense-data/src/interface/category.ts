import { Category } from '../entities/category'

export type CategoryData = {
  name: string
  parent_category?: string | Category
  description?: string
}
