import { useMemo, useState } from 'react';
import { List, Modal, Portal, TextInput, Checkbox } from 'react-native-paper';
import { FlatList, TouchableOpacity, View } from 'react-native';
import { take } from 'lodash';
// import { Touchable } from '../Touchable';
import { useTheme } from '../../theme';
import { SelectEntry, SelectOption, SelectProps } from './types';
import { ErrorText } from '../../outputs';

export const Select: React.FC<SelectProps> = ({
  value,
  onChange,
  options,
  label,
  isMultiSelection,
  disabled,
  loading,
  hasClear,
  error,
  unmountErrorWhenNoErrors,
  ...rest
}) => {
  const [showPopup, setShowPopup] = useState(false);
  const { colorScheme } = useTheme();

  const formattedValue = useMemo(() => {
    if (isMultiSelection) {
      const data = take(value, 3).map((i: any) => i?.name);
      const moreValues = value?.length - 3;
      const moreValueLabel = `${moreValues > 0 ? ` +${moreValues}` : ''}`;

      return `${data.join(', ')}${moreValueLabel}`;
    }

    return value?.name;
  }, [value, isMultiSelection]);

  const handleChange = (selectedValue: SelectOption) => {
    if (!onChange) return;

    if (isMultiSelection) {
      if (
        value?.length &&
        value.find((f: SelectOption) => f.value === selectedValue.value)
      ) {
        onChange(
          value.filter(
            (item: SelectOption) => item.value !== selectedValue.value
          )
        );
      } else {
        onChange([...(value || []), selectedValue]);
      }
    } else {
      onChange(selectedValue);
    }

    !isMultiSelection && setShowPopup(false);
  };

  return (
    <View>
      <TouchableOpacity
        disabled={disabled || loading || !options?.length}
        onPress={() => setShowPopup(true)}
        {...rest}
      >
        <TextInput
          label={label}
          value={formattedValue}
          editable={false}
          error={!!error}
          right={
            hasClear && (
              <TextInput.Icon
                icon="close"
                onPress={() => onChange && onChange(isMultiSelection ? [] : {})}
              />
            )
          }
        />
      </TouchableOpacity>
      <Portal>
        <Modal
          visible={showPopup}
          onDismiss={() => setShowPopup(false)}
          contentContainerStyle={{
            backgroundColor: colorScheme.background,
            margin: 20,
            maxHeight: '70%',
          }}
        >
          <View>
            <FlatList
              data={options}
              renderItem={({ item, index }) => (
                <Entry
                  key={index}
                  data={item}
                  onChange={handleChange}
                  isMultiSelection={isMultiSelection}
                  selectedValue={value}
                />
              )}
            />
          </View>
        </Modal>
      </Portal>

      <ErrorText
        unmountErrorWhenNoErrors={unmountErrorWhenNoErrors}
        error={error}
      />
    </View>
  );
};

const Entry: React.FC<SelectEntry> = ({
  data,
  onChange,
  selectedValue,
  isMultiSelection,
}) => {
  const { colorScheme } = useTheme();
  const isSelected = useMemo(() => {
    if (isMultiSelection) {
      return !!selectedValue?.find((f: SelectOption) => f.value === data.value);
    }

    return selectedValue?.value === data.value;
  }, [isMultiSelection, selectedValue, data.value]);

  return (
    <List.Item
      className="px-5"
      title={data.name}
      onPress={() => onChange(data)}
      left={() => <List.Icon icon="folder" />}
      style={{
        backgroundColor: isSelected ? colorScheme.primaryContainer : undefined,
      }}
      right={() =>
        isMultiSelection && (
          <Checkbox status={isSelected ? 'checked' : 'unchecked'} />
        )
      }
    />
  );
};
