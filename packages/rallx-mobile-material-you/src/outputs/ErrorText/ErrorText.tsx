import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';
import { HelperText } from 'react-native-paper';

export type ErrorTextProps = ReactNativeComponentBase<{
  error?: string;
  unmountErrorWhenNoErrors?: boolean;
}>;

export const ErrorText: React.FC<ErrorTextProps> = ({
  error,
  unmountErrorWhenNoErrors = true,
  ...rest
}) => {
  if (!error && unmountErrorWhenNoErrors) return null;

  return (
    <HelperText type="error" visible={!!error} {...rest}>
      {error}
    </HelperText>
  );
};
