// -----------------------------------------------------------------------

export const getEnv = (value?: string): string | undefined => {
  if (value) {
    return process.env[value] as string;
  }
  return undefined;
};

// -----------------------------------------------------------------------
