import { omit } from 'lodash';
import { DataSource, Repository } from 'typeorm';
import { convertToInt } from '@rallx-artis/common-functions';
import { loanType } from '../constants/loan';
import { Account } from '../entities/account';
import { Category } from '../entities/category';
import { Loan } from '../entities/loan';
import { LoanData, LoanFilter } from '../interface/loan';
import { AuthObject, RepoData } from '../interface/common';
import { Options, genPromise, insertTypeObject } from '../utils';

export class LoanController {
  private repo: Repository<Loan>;

  private conn: DataSource;

  private userInfo: AuthObject;

  constructor(args: RepoData) {
    this.repo = args.conn.getRepository(Loan);
    this.conn = args.conn;
    this.userInfo = args.authInfo;
  }

  getAllLoans = (
    options?: Options & { filter?: LoanFilter; canUseToPay?: boolean }
  ): Promise<Loan[]> => {
    const { filter, canUseToPay } = options || {};

    const otherFilter = {
      ...omit(filter || {}, 'type'),
    };

    let loanFilter = {};
    if (canUseToPay) {
      const payableLoan = loanType?.filter((type) => type.id !== 'emi');

      loanFilter = {
        where: payableLoan.map((loan) => ({
          type: loan.id,
          ...otherFilter,
        })),
      };
    } else if (filter?.type) {
      const type = filter?.type;
      loanFilter = {
        where:
          typeof type === 'string'
            ? { type }
            : type.map((ty) => ({
                type: ty,
                ...otherFilter,
              })),
      };
    } else {
      loanFilter = {
        where: otherFilter,
      };
    }

    return genPromise(
      () =>
        this.repo.find({
          ...loanFilter,
          relations: ['account'],
        }),
      options
    );
  };

  getLoan = (id: string, options?: Options): Promise<Loan> => {
    return genPromise(() => this.repo.findOne({ where: { id } }), options);
  };

  addLoan = (data: LoanData, options?: Options): Promise<unknown> => {
    const { is_autopay, is_reloadable } = data;

    return genPromise(async () => {
      const category = await insertTypeObject(
        data.category,
        this.conn,
        Category
      );
      const account = await insertTypeObject(data.account, this.conn, Account);

      const newLoan = this.repo.create({
        ...data,
        name: data?.name?.trim(),
        user_id: this.userInfo.uid,
        account,
        category,
        is_reloadable: !!is_reloadable,
        is_autopay: !!is_autopay,
        balance: convertToInt(data?.balance),
        fees: convertToInt(data?.fees),
        payment: convertToInt(data?.payment),
        total_amount: convertToInt(data?.total_amount),
        interest: convertToInt(data?.interest),
        default_reward_point: convertToInt(data?.default_reward_point),
      });
      return this.repo.save(newLoan);
    }, options);
  };

  updateLoan = (
    id: string,
    data: LoanData,
    options?: Options
  ): Promise<unknown> => {
    return genPromise(async () => {
      const { is_autopay, is_reloadable } = data;

      const category = await insertTypeObject(
        data.category,
        this.conn,
        Category
      );
      const account = await insertTypeObject(data.account, this.conn, Account);

      const updatedLoan = {
        ...data,
        name: data?.name?.trim(),
        account,
        category,
        is_reloadable: !!is_reloadable,
        is_autopay: !!is_autopay,
        balance: convertToInt(data?.balance),
        fees: convertToInt(data?.fees),
        payment: convertToInt(data?.payment),
        total_amount: convertToInt(data?.total_amount),
        interest: convertToInt(data?.interest),
        default_reward_point: convertToInt(data?.default_reward_point),
      };

      return this.repo.update({ id }, updatedLoan);
    }, options);
  };

  removeLoan = (id: string, options?: Options): Promise<unknown> => {
    return genPromise(() => this.repo.delete({ id }), options);
  };
}
