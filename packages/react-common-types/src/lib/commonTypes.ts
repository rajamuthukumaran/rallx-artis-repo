import type { ReactNode, MouseEvent, Dispatch, SetStateAction } from 'react';

export type ReactChild = {
  children?: ReactNode;
};

export type ElementOnClick = (event: MouseEvent) => void;

export type StateCallback<t> = (value: t) => void | Dispatch<SetStateAction<t>>;

export type CommonInputComponentBase = {
  loading?: boolean;
  disabled?: boolean;
  name?: string;
};
