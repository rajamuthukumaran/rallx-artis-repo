export enum LoanTypes {
  'EMI' = 'emi',
  'credit_card' = 'credit_card',
  'loan' = 'loan',
}

export const loanType = [
  {
    id: 'emi',
    name: 'EMI',
  },
  {
    id: 'credit_card',
    name: 'Credit card',
    isLoan: true,
  },
  {
    id: 'loan',
    name: 'Loan',
    isLoan: true,
  },
]

export const getLoanType = loanType.reduce((a, entry) => {
  return {
    ...a,
    [entry.id]: entry.name,
  }
}, {})
