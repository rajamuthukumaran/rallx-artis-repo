import React from 'react';
import { IconButton as RNIconButton } from 'react-native-paper';
import { useTheme } from '../../theme';
import {
  OnPressCallback,
  ReactNativeComponentBase,
} from '@rallx-artis/react-native-common-types';
import { CommonInputComponentBase } from '@rallx-artis/react-common-types';

export type IconButtonProps = ReactNativeComponentBase<{
  icon: string;
  color?: string;
  size?: number;
  variant?: 'transparent' | 'solid';
  onPress?: OnPressCallback;
}> &
  CommonInputComponentBase;

const ICON_BUTTON_MODE = {
  transparent: undefined,
  solid: 'contained',
} as const;

export const IconButton: React.FC<IconButtonProps> = ({
  icon,
  color,
  size,
  variant = 'transparent',
  disabled,
  loading,
  ...rest
}) => {
  const { colorScheme } = useTheme();

  return (
    <RNIconButton
      disabled={disabled || loading}
      mode={ICON_BUTTON_MODE[variant]}
      icon={icon}
      iconColor={color || colorScheme.onSurface}
      size={size || 24}
      {...rest}
    />
  );
};
