import { useFonts } from 'expo-font';

export const usePoppins = () => {
  const fontDetails = useFonts({
    Poppins: require('../../../../../assets/fonts/poppins/Poppins-Regular.ttf'),
    Poppins_medium: require('../../../../../assets/fonts/poppins/Poppins-Medium.ttf'),
    Poppins_semibold: require('../../../../../assets/fonts/poppins/Poppins-SemiBold.ttf'),
    Poppins_bold: require('../../../../../assets/fonts/poppins/Poppins-Bold.ttf'),
    Poppins_extrabold: require('../../../../../assets/fonts/poppins/Poppins-ExtraBold.ttf'),
    Poppins_black: require('../../../../../assets/fonts/poppins/Poppins-Black.ttf'),
    Poppins_thin: require('../../../../../assets/fonts/poppins/Poppins-Thin.ttf'),
    Poppins_light: require('../../../../../assets/fonts/poppins/Poppins-Light.ttf'),
  });

  return fontDetails;
};

export const usePoppinsItalic = () => {
  const fontDetails = useFonts({
    Poppins_italic: require('../../../../../assets/fonts/poppins/Poppins-Italic.ttf'),
    Poppins_medium_italic: require('../../../../../assets/fonts/poppins/Poppins-MediumItalic.ttf'),
    Poppins_semibold_italic: require('../../../../../assets/fonts/poppins/Poppins-SemiBoldItalic.ttf'),
    Poppins_bold_italic: require('../../../../../assets/fonts/poppins/Poppins-BoldItalic.ttf'),
    Poppins_extrabold_italic: require('../../../../../assets/fonts/poppins/Poppins-ExtraBoldItalic.ttf'),
    Poppins_black_italic: require('../../../../../assets/fonts/poppins/Poppins-BlackItalic.ttf'),
  });

  return fontDetails;
};
