import { ColorScheme, Fonts, ThemeMode } from '@rallx-artis/common-types';
import { createContext, memo, useContext, useEffect, useState } from 'react';
import { ReactChild, StateCallback } from '@rallx-artis/react-common-types';

export type ThemeContextProps = {
  theme?: {
    primary?: ColorScheme;
    secondary?: ColorScheme;
    dark?: Record<string, any>;
    light?: Record<string, any>;
  };
  themeMode?: ThemeMode;
  fonts?: Fonts;
  fontFamily?: string;
};

const ThemeContext = createContext<{
  themeConfig: ThemeContextProps;
  setThemeConfig: StateCallback<ThemeContextProps>;
}>({
  themeConfig: {},
  setThemeConfig: () => undefined,
});

export const ThemeProvider: React.FC<
  ReactChild & { data?: ThemeContextProps }
> = memo(({ children, data }) => {
  const [themeConfig, setThemeConfig] = useState<ThemeContextProps>(data || {});

  useEffect(() => {
    setThemeConfig(data || {});
  }, [data]);

  return (
    <ThemeContext.Provider value={{ themeConfig, setThemeConfig }}>
      {children}
    </ThemeContext.Provider>
  );
});

export const useTheme = () => {
  return useContext(ThemeContext);
};
