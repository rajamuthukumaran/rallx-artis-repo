import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { DataSource } from 'typeorm';
import { useAuth } from '../context/Authendication';
// import { MoneyLoader } from '../components/FullscreenLoader'
import { ReactChild } from '@rallx-artis/react-common-types';
import {
  Account,
  AccountController,
  Category,
  CategoryController,
  DataConn,
  Expense,
  ExpenseController,
  Loan,
  LoanController,
  RepoData,
} from '@rallx-artis/rallx-expense-data';
import { logError } from '@rallx-artis/common-functions';
import { Text, View } from 'react-native';
import * as SQLite from 'expo-sqlite/legacy';

const DataConnContext = React.createContext<DataConn>({} as DataConn);

const DataProvider: React.FC<ReactChild> = ({
  children,
}): React.ReactElement => {
  const [conn, setConn] = useState<DataSource | null>(null);
  const authInfo = useAuth();

  const repoData: RepoData = useMemo(
    () => ({
      conn: conn!,
      authInfo,
    }),
    [conn, authInfo]
  );

  const connect = useCallback(async () => {
    try {
      const res = new DataSource({
        type: 'expo',
        database: `rallx_expense.db`,
        driver: SQLite,
        entities: [Account, Expense, Loan, Category],
        synchronize: true,
        logging: true,
      });

      res
        .initialize()
        .then(() => {
          setConn(res);
        })
        .catch((err) => logError(err));
    } catch (e) {
      logError(e);
      setConn(null);
    }
  }, []);

  useEffect(() => {
    if (!conn) {
      connect();
    }

    return () => conn && (conn.destroy() as any);
  }, [conn, connect]);

  const repos = useMemo(() => {
    if (conn) {
      return {
        accountRepo: new AccountController(repoData),
        expenseRepo: new ExpenseController(repoData),
        loanRepo: new LoanController(repoData),
        categoryRepo: new CategoryController(repoData),
        conn,
      };
    }

    return undefined;
  }, [conn, repoData]);

  if (!conn) {
    // return <MoneyLoader />
    return (
      <View>
        <Text>Loading...</Text>
      </View>
    );
  }

  return (
    <DataConnContext.Provider value={repos!}>
      {children}
    </DataConnContext.Provider>
  );
};

export const useDatabase = () => {
  const conn = useContext(DataConnContext);

  return conn;
};

export default DataProvider;
