import { BackButton as Back } from '@rallx-artis/rallx-mobile-material-you';
import { useRouter } from 'expo-router';
import { Platform } from 'react-native';

export const BackButton: React.FC = () => {
  const router = useRouter();

  return <Back onPress={() => router.back()} />;
};

export const HeaderBackButton =
  Platform.OS === 'ios' ? () => <BackButton /> : undefined;
