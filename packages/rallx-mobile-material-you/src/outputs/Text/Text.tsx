import { Text as RNText } from 'react-native-paper';
import { forwardRef } from 'react';
import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';
import { textEllipsis } from '@rallx-artis/common-functions';

export type TextProps = {
  maxLength?: number;
  children?: string;
} & Omit<ReactNativeComponentBase, 'children'>;

export const Text: React.FC<TextProps> = forwardRef(
  ({ children, maxLength, ...rest }, ref: any) => {
    return (
      <RNText ref={ref} {...rest}>
        {textEllipsis(children, maxLength)}
      </RNText>
    );
  }
);
