import { TouchableOpacity, View } from 'react-native';
import { TimePickerModal } from 'react-native-paper-dates';
import React, { useMemo, useState } from 'react';
import { TimePickerFieldProps, TimePickerObject } from './types';
import { TextInput } from 'react-native-paper';
import { ErrorText } from '../../outputs';
import { prefixZeroToInt } from '@rallx-artis/common-functions';
import { isEmpty } from 'lodash';

export const TimePickerField: React.FC<TimePickerFieldProps> = ({
  value,
  onChange,
  disabled,
  loading,
  error,
  hasError,
  className,
  is24Hour = false,
  ...rest
}) => {
  const [openPopup, setOpenPopup] = useState(false);

  const formattedValue = useMemo(() => {
    if (!isEmpty(value))
      return `${prefixZeroToInt(value?.hours)}:${prefixZeroToInt(
        value?.minutes
      )}`;

    return '';
  }, [value]);

  const handleChange = (data: TimePickerObject) => {
    onChange && onChange(data);
    setOpenPopup(false);
  };

  return (
    <View className={className}>
      <TouchableOpacity
        disabled={disabled || loading}
        onPress={() => setOpenPopup(true)}
      >
        <TextInput
          value={formattedValue}
          editable={false}
          error={!!error || hasError}
          placeholder="HH:MM"
          right={
            <TextInput.Icon
              icon="close"
              onPress={() => onChange && onChange({})}
            />
          }
          {...rest}
        />
      </TouchableOpacity>
      <TimePickerModal
        hours={value?.hours}
        minutes={value?.minutes}
        locale="en-GB"
        visible={openPopup}
        onDismiss={() => setOpenPopup(false)}
        onConfirm={handleChange}
        use24HourClock={is24Hour}
      />
      <ErrorText error={error} />
    </View>
  );
};
