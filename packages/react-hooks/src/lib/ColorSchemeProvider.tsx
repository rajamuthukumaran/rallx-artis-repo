import { ReactChild, StateCallback } from '@rallx-artis/react-common-types';
import { createContext, useContext, useState } from 'react';

const ColorSchemeContext = createContext<{
  isDark: boolean;
  setDark: StateCallback<boolean>;
}>({
  isDark: true,
  setDark: () => undefined,
});

export const ColorSchemeProvider: React.FC<ReactChild> = ({ children }) => {
  const [isDark, setDark] = useState(true);

  return (
    <ColorSchemeContext.Provider
      value={{
        isDark,
        setDark,
      }}
    >
      {children}
    </ColorSchemeContext.Provider>
  );
};

export const useDarkTheme = () => {
  return useContext(ColorSchemeContext);
};
