import { useFocusEffect } from '@react-navigation/core'
import { DependencyList, EffectCallback, useCallback } from 'react'

export type UseMountEffectProps = (
  callback: EffectCallback,
  dependency: DependencyList,
) => void

export const useMountEffect: UseMountEffectProps = (callback, dependency) => {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useFocusEffect(useCallback(callback, dependency))
}

