import {
  Control,
  FieldErrors,
  FieldValues,
  RegisterOptions,
} from 'react-hook-form';
import {
  CheckBoxProps,
  DatePickerFieldProps,
  InputFieldProps,
  RadioButtonsProps,
  SelectProps,
} from '../inputs';

export type FormFieldCommonProps<T extends FieldValues = any> = {
  name: string;
  control: Control<T>;
  defaultValue?: any;
  rules?: Exclude<
    RegisterOptions,
    'valueAsNumber' | 'valueAsDate' | 'setValueAs'
  >;
  errors?: FieldErrors['root'][];
  isRequired?: boolean;
};

export type FormInputFieldProps = FormFieldCommonProps & InputFieldProps;

export type FormSelectFieldProps = FormFieldCommonProps & SelectProps;

export type FormCheckboxProps = FormFieldCommonProps & CheckBoxProps;

export type FormRadioGroupProps = FormFieldCommonProps & RadioButtonsProps;

export type FormDatePickerProps = FormFieldCommonProps & DatePickerFieldProps;
