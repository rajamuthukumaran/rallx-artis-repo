import { View } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import { Text } from '../../outputs';
import {
  OnPressCallback,
  ReactNativeComponentBase,
} from '@rallx-artis/react-native-common-types';
import {
  CommonInputComponentBase,
  ReactChild,
} from '@rallx-artis/react-common-types';
import { useTheme } from '../../theme';
import { MaterialIcons } from '@expo/vector-icons';

export type MenuButtonProps = {
  title?: string;
  /**
   * Expo icon component used for menu default material icons.
   */
  iconComponent?: any;
  icon?: string;
  color?: string;
  onPress?: OnPressCallback;
  children?: string;
} & Omit<ReactNativeComponentBase, 'children'> &
  CommonInputComponentBase;

export const MenuButton: React.FC<MenuButtonProps> = ({
  onPress,
  icon,
  title,
  children,
  disabled,
  loading,
  className,
  iconComponent: IconComponent = MaterialIcons,
  color,
  ...rest
}) => {
  const { colorScheme } = useTheme();

  return (
    <TouchableRipple disabled={disabled || loading} onPress={onPress} {...rest}>
      <View className="flex-row gap-5 p-2 rounded-lg">
        {!!icon && (
          <IconComponent
            name={icon}
            color={color || colorScheme.secondary}
            size={32}
          />
        )}
        <Text className="text-xl mt-1">{title || children}</Text>
      </View>
    </TouchableRipple>
  );
};
