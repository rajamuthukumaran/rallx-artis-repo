import { Stack } from 'expo-router';
import { HeaderBackButton } from '../../components/BackButton';

const Layout = () => {
  return (
    <Stack
      screenOptions={{
        presentation: 'modal',
        headerLeft: HeaderBackButton,
      }}
    >
      <Stack.Screen name="AddExpense" options={{ title: 'Add Expense' }} />
      <Stack.Screen name="AddCategory" options={{ title: 'Add Category' }} />
    </Stack>
  );
};

export default Layout;
