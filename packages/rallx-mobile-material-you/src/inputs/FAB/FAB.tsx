import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import {
  OnPressCallback,
  ReactNativeComponentBase,
} from '@rallx-artis/react-native-common-types';
import { FAB as RNFAB } from 'react-native-paper';

export type FABProps = ReactNativeComponentBase<{
  icon?: any;
  variant?: 'primary' | 'secondary' | 'tertiary' | 'surface';
  onPress?: OnPressCallback;
  onLongPress?: OnPressCallback;
  color?: string;
  backgroundColor?: string;
  size?: 'sm' | 'm' | 'lg';
  label?: string;
  visible?: boolean;
}> &
  CommonInputComponentBase;

const standardSizeConverter = {
  sm: 'small',
  m: 'medium',
  lg: 'large',
} as const;

export const FAB: React.FC<FABProps> = ({
  backgroundColor,
  size,
  label,
  ...rest
}) => {
  return (
    <RNFAB
      size={size ? standardSizeConverter[size] : undefined}
      theme={{
        colors: {
          background: backgroundColor,
        },
      }}
      label={label || ''}
      {...rest}
    />
  );
};
