import * as dayjs from 'dayjs';
import { DateRange, DateString, Range } from '@rallx-artis/common-types';
import { ExpenseType } from '../constants/expense';
import { Account } from '../entities/account';
import { Category } from '../entities/category';
import { Expense } from '../entities/expense';
import { Loan } from '../entities/loan';

export type ExpenseProcess = ExpenseType.income | ExpenseType.expense;

export type AccountAmt = {
  account?: string | Account;
  loan?: string | Loan;
  amount: number | string;
  rewardPercent?: number;
  transfer_account?: string | Account;
  transfer_loan?: string | Loan;
  process: ExpenseProcess;
  doesnt_count?: boolean;
};

export type Transaction = {
  id?: string;
  name: string;
  category?: string | Category;
  date: DateString;
  description?: string;
  accounts?: AccountAmt[];
};

export type UpdateTransaction = Omit<
  Expense,
  'transfer_account' | 'transfer_loan' | 'account' | 'loan' | 'transaction'
> & {
  transfer_account: string | Account;
  account: string | Account;
  transfer_loan: string | Loan;
  loan: string | Loan;
  transaction: string | number;
};

export type BalancePeriod = {
  period?: dayjs.UnitType;
  date?: DateRange;
  includeAllBal?: boolean;
};

export type TransactionBalance = {
  income: number;
  expense: number;
  transfer: number;
};

export type ExpenseFilter = {
  name?: string;
  type?: ExpenseType;
  transaction?: Range;
  date?: DateRange;
  category?: Category;
  account?: Account;
  period?: dayjs.UnitType;
};
