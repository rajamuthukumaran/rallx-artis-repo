import { ReactChild } from '@rallx-artis/react-common-types';
import { View } from 'react-native';
import Popover from 'react-native-popover-view';
import { Text } from '../../outputs';
import { TouchableRipple } from 'react-native-paper';
import { useTheme } from '../../theme';
import { useState } from 'react';
import { IconButton } from '../../inputs';

export type PopoverConfirmationProps = ReactChild & {
  onConfirm: () => void;
  message?: string;
};

export const PopoverConfirmation: React.FC<PopoverConfirmationProps> = ({
  onConfirm,
  children,
  message = 'Are you sure?',
}) => {
  const { colorScheme } = useTheme();
  const [showPopover, setShowPopover] = useState(false);

  return (
    <Popover
      isVisible={showPopover}
      onRequestClose={() => setShowPopover(false)}
      popoverStyle={{
        backgroundColor: colorScheme.background,
        borderRadius: 15,
      }}
      from={
        <TouchableRipple onPress={() => setShowPopover(true)}>
          {children}
        </TouchableRipple>
      }
    >
      <View
        className="p-4 min-w-32 gap-5 justify-center"
        style={{
          backgroundColor: colorScheme.background,
        }}
      >
        <Text>{message}</Text>
        <View className="flex-row justify-between gap-2">
          <IconButton icon="check" color="red" onPress={onConfirm} />

          <IconButton
            icon="close"
            color="green"
            onPress={() => setShowPopover(false)}
          />
        </View>
      </View>
    </Popover>
  );
};

export default PopoverConfirmation;
