import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';

export type CheckBoxProps = ReactNativeComponentBase<{
  checked?: boolean;
  label?: string;
  color?: string;
  error?: string;
  onChange?: (value: boolean) => void;
}> &
  CommonInputComponentBase;
