import { ExpenseProcess } from '../interface';
import { ExpenseType } from '../constants';
import { NumChar } from '@rallx-artis/common-types';
import { convertToInt } from '@rallx-artis/common-functions';

export const calcTransaction = (
  balance: NumChar,
  transaction: NumChar,
  type: ExpenseProcess
) => {
  const calcBal = convertToInt(balance);
  const calcTrx = convertToInt(transaction);

  if (type === ExpenseType.income) {
    return calcBal + calcTrx;
  }
  return calcBal - calcTrx;
};

// -----------------------------------------------------------------------

// prettier-ignore
export const updateExistingTransaction = (
  balance: NumChar,
  newTransaction: NumChar,
  oldTransaction: NumChar,
  type: ExpenseProcess,
) => {
  const calcBal = convertToInt(balance)
  const newTran = convertToInt(newTransaction)
  const oldTran = convertToInt(oldTransaction)

  const transaction = newTran - oldTran

  if (type === ExpenseType.income) {
    return calcBal + transaction
  }
  return calcBal - transaction
}

// -----------------------------------------------------------------------
