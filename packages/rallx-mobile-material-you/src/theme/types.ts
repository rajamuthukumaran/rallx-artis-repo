import { ReactChild } from '@rallx-artis/react-common-types';
import { ThemeContextProps } from '@rallx-artis/react-hooks';

export type ThemeProps = ThemeContextProps & ReactChild;
