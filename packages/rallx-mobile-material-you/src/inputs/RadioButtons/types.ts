import { optionProps } from '@rallx-artis/common-types';
import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';

type ListDirection = 'horizontal' | 'vertical';

export type RadioButtonsProps = ReactNativeComponentBase<{
  options: optionProps[];
  value?: any;
  onChange: (value: any) => void;
  listDirection?: ListDirection;
}> &
  CommonInputComponentBase;

export type RadioButtonProps = ReactNativeComponentBase<
  optionProps & { disabled?: boolean }
>;
