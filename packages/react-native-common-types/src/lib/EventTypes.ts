import type { GestureResponderEvent } from 'react-native';

export type OnPressCallback = (e: GestureResponderEvent) => void;
