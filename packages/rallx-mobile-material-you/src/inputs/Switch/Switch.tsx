import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import { View } from 'react-native';
import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';
import { Switch as RNSwitch } from 'react-native-paper';
import { Text } from '../../outputs';

export type SwitchProps = ReactNativeComponentBase<{
  checked?: boolean;
  label?: string;
  onChange?: (value: boolean) => void;
}> &
  CommonInputComponentBase;

export const Switch: React.FC<SwitchProps> = ({
  checked,
  label,
  onChange,
  ...rest
}) => {
  return (
    <View>
      <View className="flex-row items-center">
        <RNSwitch
          value={checked}
          onValueChange={() => onChange && onChange(!checked)}
          {...rest}
        />
        {label && <Text>{label}</Text>}
      </View>
    </View>
  );
};
