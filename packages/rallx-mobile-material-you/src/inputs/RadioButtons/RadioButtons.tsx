import { View } from 'react-native';
import { RadioButton as RNRadioButton } from 'react-native-paper';
import { clsx } from 'clsx';
import { RadioButtonProps, RadioButtonsProps } from './types';
import { Text } from '../../outputs';

export const RadioButtons: React.FC<RadioButtonsProps> = ({
  onChange,
  disabled,
  loading,
  options,
  value,
  listDirection,
  ...rest
}) => {
  return (
    <View {...rest}>
      <RNRadioButton.Group value={value} onValueChange={onChange}>
        <View
          className={clsx(
            listDirection === 'horizontal'
              ? 'flex-row items-center gap-2'
              : 'flex-column justify-center'
          )}
        >
          {options.map((option) => (
            <RadioButton
              key={option.name}
              disabled={disabled || loading}
              {...option}
            />
          ))}
        </View>
      </RNRadioButton.Group>
    </View>
  );
};

export const RadioButton: React.FC<RadioButtonProps> = ({ name, ...rest }) => {
  return (
    <View className={'flex-row items-center'}>
      <RNRadioButton {...rest} />
      <Text className="mt-1">{name}</Text>
    </View>
  );
};
