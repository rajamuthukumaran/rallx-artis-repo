import { Platform, View } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';
import { Text } from '../../outputs';
import {
  OnPressCallback,
  ReactNativeComponentBase,
} from '@rallx-artis/react-native-common-types';
import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import { useTheme } from '../../theme';

export type BackButtonProps = ReactNativeComponentBase<
  {
    onPress?: OnPressCallback;
  } & Omit<CommonInputComponentBase, 'name'>
>;

export const BackButton: React.FC<BackButtonProps> = ({
  children,
  disabled,
  loading,
  ...rest
}) => {
  const { colorScheme } = useTheme();

  return (
    <TouchableRipple disabled={disabled || loading} {...rest}>
      <View className="flex-row items-center gap-1">
        <Ionicons
          name="chevron-back-sharp"
          size={16}
          color={colorScheme.primary}
        />
        {children || <Text style={{ color: colorScheme.primary }}>Back</Text>}
      </View>
    </TouchableRipple>
  );
};

export const HeaderBackButton = () =>
  Platform.OS === 'ios' ? () => <BackButton /> : undefined;

export default BackButton;
