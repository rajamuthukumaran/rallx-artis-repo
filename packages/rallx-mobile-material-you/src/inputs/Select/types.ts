import { optionProps } from '@rallx-artis/common-types';
import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';

export type SelectOption = optionProps & {
  icon?: string;
  //   section?: string;
};

export type SelectProps = ReactNativeComponentBase<{
  value?: any;
  onChange?: (value: any) => void;
  options?: SelectOption[];
  label?: string;
  //   hasSections?: boolean;
  hasClear?: boolean;
  isMultiSelection?: boolean;
  error?: string;
  unmountErrorWhenNoErrors?: boolean;
}> &
  CommonInputComponentBase;

export type SelectEntry = {
  data: SelectOption;
  onChange: (e: SelectOption) => void;
  isMultiSelection?: boolean;
  selectedValue?: any;
};
