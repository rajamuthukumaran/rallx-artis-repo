import { Snackbar } from 'react-native-paper';
import { useTheme } from '../../theme';
import { Text } from '../Text';
import { OnPressCallback } from '@rallx-artis/react-native-common-types';

export type ToasterProps = {
  onDismiss: () => void;
  message?: string;
  action?: string;
  onPress?: OnPressCallback;
  variant?: 'default' | 'error';
};

export const Toaster: React.FC<ToasterProps> = ({
  action,
  message,
  onDismiss,
  onPress,
  variant,
}) => {
  const { colorScheme } = useTheme();

  const TOASTER_THEME = {
    default: {
      backgroundColor: undefined,
      color: undefined,
    },
    error: {
      backgroundColor: colorScheme.errorContainer,
      color: colorScheme.error,
    },
  } as const;

  return (
    <Snackbar
      visible={!!message}
      onDismiss={onDismiss}
      action={{
        label: action || 'Okay',
        onPress: onPress || onDismiss,
      }}
      style={{
        backgroundColor: TOASTER_THEME[variant || 'default'].backgroundColor,
      }}
    >
      <Text style={{ color: TOASTER_THEME[variant || 'default'].color }}>
        {message}
      </Text>
    </Snackbar>
  );
};

export default Toaster;
