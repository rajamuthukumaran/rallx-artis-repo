import { TouchableOpacity, View } from 'react-native';
import { DatePickerModal } from 'react-native-paper-dates';
import React, { useMemo, useState } from 'react';
import { DatePickerFieldProps } from './types';
import dayjs from 'dayjs';
import { TextInput } from 'react-native-paper';
import { ErrorText } from '../../outputs';

export const DatePickerField: React.FC<DatePickerFieldProps> = ({
  value,
  onChange,
  disabled,
  loading,
  error,
  validDates = 'any',
  className,
  hasError,
  unmountErrorWhenNoErrors,
  ...rest
}) => {
  const [openPopup, setOpenPopup] = useState(false);

  const formattedValue = useMemo(() => {
    if (value) return dayjs(value).format('DD/MM/YYYY');

    return '';
  }, [value]);

  const processedValidDates = useMemo(() => {
    if (typeof validDates === 'object') {
      return validDates;
    }

    if (validDates === 'future') {
      return {
        startDate: new Date(),
      };
    }

    if (validDates === 'past') {
      return {
        endDate: new Date(),
      };
    }

    return undefined;
  }, [validDates]);

  const handleChange = (date?: Date) => {
    onChange && onChange(date);
    setOpenPopup(false);
  };

  return (
    <View className={className}>
      <TouchableOpacity
        disabled={disabled || loading}
        onPress={() => setOpenPopup(true)}
      >
        <TextInput
          value={formattedValue}
          editable={false}
          error={!!error || hasError}
          placeholder="DD/MM/YYYY"
          right={
            <TextInput.Icon
              icon="close"
              onPress={() => onChange && onChange(undefined)}
            />
          }
          {...rest}
        />
      </TouchableOpacity>
      <DatePickerModal
        date={value}
        validRange={processedValidDates as any}
        locale="en-GB"
        mode={'single'}
        visible={openPopup}
        onDismiss={() => setOpenPopup(false)}
        onConfirm={({ date }) => handleChange(date)}
        presentationStyle="pageSheet"
      />
      <ErrorText
        unmountErrorWhenNoErrors={unmountErrorWhenNoErrors}
        error={error}
      />
    </View>
  );
};
