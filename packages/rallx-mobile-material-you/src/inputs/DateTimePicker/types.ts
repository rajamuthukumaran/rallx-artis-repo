import { DateRange } from '@rallx-artis/common-types';
import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';

export type DatePickerFieldProps = ReactNativeComponentBase<{
  value?: Date;
  onChange?: (value?: Date) => void;
  label?: string;
  error?: string;
  validDates?: 'any' | 'future' | 'past' | DateRange;
  unmountErrorWhenNoErrors?: boolean;
  hasError?: boolean;
  // mode?: 'single' | 'multiple' | 'range';
}> &
  CommonInputComponentBase;

export type TimePickerObject = {
  hours?: number;
  minutes?: number;
};

export type TimePickerFieldProps = ReactNativeComponentBase<{
  value?: TimePickerObject;
  onChange?: (params: TimePickerObject) => void;
  label?: string;
  error?: string;
  unmountErrorWhenNoErrors?: boolean;
  is24Hour?: boolean;
  hasError?: boolean;
}> &
  CommonInputComponentBase;

export type DateTimePickerProps = Omit<DatePickerFieldProps, 'hasError'> & {
  label?: string;
  dateLable?: string;
  timeLable?: string;
  fieldContainerClassName?: string;
};
