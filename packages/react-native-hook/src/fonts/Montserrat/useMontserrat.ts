import { useFonts } from 'expo-font';

export const useMontserrat = () => {
  const fontDetails = useFonts({
    Montserrat: require('../../../../../assets/fonts/Montserrat/Montserrat-Regular.ttf'),
    Montserrat_medium: require('../../../../../assets/fonts/Montserrat/Montserrat-Medium.ttf'),
    Montserrat_semibold: require('../../../../../assets/fonts/Montserrat/Montserrat-SemiBold.ttf'),
    Montserrat_bold: require('../../../../../assets/fonts/Montserrat/Montserrat-Bold.ttf'),
    Montserrat_extrabold: require('../../../../../assets/fonts/Montserrat/Montserrat-ExtraBold.ttf'),
    Montserrat_black: require('../../../../../assets/fonts/Montserrat/Montserrat-Black.ttf'),
    Montserrat_thin: require('../../../../../assets/fonts/Montserrat/Montserrat-Thin.ttf'),
    Montserrat_light: require('../../../../../assets/fonts/Montserrat/Montserrat-Light.ttf'),
  });

  return fontDetails;
};

export const useMontserratItalic = () => {
  const fontDetails = useFonts({
    Montserrat_italic: require('../../../../../assets/fonts/Montserrat/Montserrat-Italic.ttf'),
    Montserrat_medium_italic: require('../../../../../assets/fonts/Montserrat/Montserrat-MediumItalic.ttf'),
    Montserrat_semibold_italic: require('../../../../../assets/fonts/Montserrat/Montserrat-SemiBoldItalic.ttf'),
    Montserrat_bold_italic: require('../../../../../assets/fonts/Montserrat/Montserrat-BoldItalic.ttf'),
    Montserrat_extrabold_italic: require('../../../../../assets/fonts/Montserrat/Montserrat-ExtraBoldItalic.ttf'),
    Montserrat_black_italic: require('../../../../../assets/fonts/Montserrat/Montserrat-BlackItalic.ttf'),
  });

  return fontDetails;
};
