import { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import {
  OnPressCallback,
  ReactNativeComponentBase,
} from '@rallx-artis/react-native-common-types';
import { TouchableRipple } from 'react-native-paper';

export type TouchableProps = ReactNativeComponentBase & {
  onPress?: OnPressCallback;
  onLongPress?: OnPressCallback;
  rippleColor?: string;
} & CommonInputComponentBase;

export const Touchable: React.FC<TouchableProps> = ({
  disabled,
  loading,
  children,
  ...rest
}) => (
  <TouchableRipple disabled={disabled || loading} {...rest}>
    {children}
  </TouchableRipple>
);
