export type NumChar = string | number;

export type DateString = string | Date;

export type DateRange = {
  startDate: DateString;
  endDate?: DateString;
};

export type Period = {
  start?: DateString;
  end?: DateString;
};

export type NumRange = {
  min?: number;
  max?: number;
};

export type Range = {
  min?: NumChar;
  max?: NumChar;
};

export type optionProps = {
  value: any;
  name: string;
};
