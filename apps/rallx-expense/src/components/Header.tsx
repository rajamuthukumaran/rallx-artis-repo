import { Text, useTheme } from '@rallx-artis/rallx-mobile-material-you';
import { View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const Header: React.FC = () => {
  const { colorScheme } = useTheme();
  const { top } = useSafeAreaInsets();

  return (
    <View
      style={{
        paddingTop: top,
        backgroundColor: colorScheme.background,
      }}
    >
      <View
        className="p-4 flex-row justify-between items-center"
        style={{
          backgroundColor: colorScheme.background,
        }}
      >
        <Text
          style={{
            color: colorScheme.secondary,
            fontFamily: 'Montserrat_medium',
          }}
          className="text-4xl"
        >
          Rallx
        </Text>
        <View className="items-end">
          <Text>Rajamuthukumaran D</Text>
          <Text style={{ color: colorScheme.primary }}>₹ 10,00,00,000</Text>
        </View>
      </View>
    </View>
  );
};

export default Header;
