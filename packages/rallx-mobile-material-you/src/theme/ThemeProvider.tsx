import UIProvider from './UIProvider';
import { ThemeProps } from './types';
import { ThemeProvider as ThemeContextProvider } from '@rallx-artis/react-hooks';

export const ThemeProvider: React.FC<ThemeProps> = ({ children, ...rest }) => {
  return (
    <ThemeContextProvider data={rest}>
      <UIProvider>{children}</UIProvider>
    </ThemeContextProvider>
  );
};
