import { useState } from 'react';
import { ToasterProps } from './Toaster';

export type ToasterObj = Pick<
  ToasterProps,
  'message' | 'variant' | 'action' | 'onPress'
>;

export const useToaster = () => {
  const [toasterData, setToaster] = useState<ToasterObj>({});

  return {
    setToaster,
    toasterConfig: {
      ...toasterData,
      onDismiss: () => setToaster({}),
    },
  };
};
