import { Text } from '@rallx-artis/rallx-mobile-material-you';
import React from 'react';
import { ReactChild } from '@rallx-artis/react-common-types';
import { View } from 'react-native';

export const PageContainer: React.FC<ReactChild & { title: string }> = ({
  title,
  children,
}) => {
  return (
    <View>
      <View className="px-4">
        <Text className="text-4xl my-2">{title}</Text>
        {children}
      </View>
    </View>
  );
};
