import { MenuButton } from '@rallx-artis/rallx-mobile-material-you';
import { PageContainer } from '../../components/ScreenLayouts';
import { useRouter } from 'expo-router';
import { Entypo } from '@expo/vector-icons';
import { View } from 'react-native';

const Settings: React.FC = () => {
  const router = useRouter();

  return (
    <PageContainer title="Settings">
      <View className="gap-4 mt-8">
        <MenuButton
          title="Category"
          icon="category"
          onPress={() => router.push('AddCategory')}
        />
        <MenuButton
          title="Data"
          iconComponent={Entypo}
          icon="database"
          onPress={() => router.push('AddCategory')}
        />
        <MenuButton
          title="Error Logs"
          iconComponent={Entypo}
          icon="bug"
          onPress={() => router.push('AddCategory')}
        />
      </View>
    </PageContainer>
  );
};

export default Settings;
