import { DataSource } from 'typeorm';
import {
  AccountController,
  CategoryController,
  ExpenseController,
  LoanController,
} from '../controller';

export type AuthObject = {
  uid: string;
  name?: string;
  email?: string;
  isAuthendicated: boolean;
  isCheckingAuth: boolean;
};

export type DataConn = {
  accountRepo: AccountController;
  expenseRepo: ExpenseController;
  loanRepo: LoanController;
  categoryRepo: CategoryController;
  conn: DataSource | null;
};

export type RepoData = {
  conn: DataSource;
  authInfo: AuthObject;
};
