import type { CommonInputComponentBase } from '@rallx-artis/react-common-types';
import type { KeyboardType } from 'react-native';
import type { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';

export type InputFieldProps = ReactNativeComponentBase<{
  type?: 'outlined' | 'flat';
  label?: string;
  placeholder?: string;
  value?: string;
  onChangeText?: (text: string) => void;
  onBlur?: (e: any) => void;
  onFocus?: (e: any) => void;
  rightIcon?: any;
  leftIcon?: any;
  error?: string;
  keyboardType?: KeyboardType;
  maxLength?: number;
  showLength?: boolean;
  multiline?: boolean;
  editable?: boolean;
  unmountErrorWhenNoErrors?: boolean;
}> &
  CommonInputComponentBase;
