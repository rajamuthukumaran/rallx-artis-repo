export enum ExpenseType {
  'income' = 'income',
  'expense' = 'expense',
  'transfer' = 'transfer',
}

export const expenseType = [
  {
    id: 'income',
    name: 'Income',
  },
  {
    id: 'expense',
    name: 'Expense',
  },
  {
    id: 'transfer',
    name: 'Transfer or CC payment',
  },
]

export const getExpenseType = expenseType.reduce((a, entry) => {
  return {
    ...a,
    [entry.id]: entry.name,
  }
}, {})
