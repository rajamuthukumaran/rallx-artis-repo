import { Controller } from 'react-hook-form';
import {
  FormCheckboxProps,
  FormDatePickerProps,
  FormInputFieldProps,
  FormRadioGroupProps,
  FormSelectFieldProps,
} from './types';
import {
  Checkbox,
  DatePickerField,
  NumberField,
  RadioButtons,
  Select,
  TextField,
} from '../inputs';
import { get } from 'lodash';

// -------------------- Text Field -------------------------------------

export const FormTextField: React.FC<FormInputFieldProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value, onBlur } }) => (
        <TextField
          {...rest}
          onBlur={onBlur}
          value={value}
          onChangeText={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  );
};

// ----------------- Number field ------------------------------------------

export const FormNumberField: React.FC<FormInputFieldProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value, onBlur } }) => (
        <NumberField
          {...rest}
          onBlur={onBlur}
          value={value}
          onChangeText={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  );
};

// -------------------- Select ------------------------------------------

export const FormSelectField: React.FC<FormSelectFieldProps> = ({
  name,
  control,
  defaultValue,
  rules,
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <Select
          {...rest}
          value={value}
          onChange={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  );
};

// -------------------- Checkbox -------------------------------------

export const FormCheckbox: React.FC<FormCheckboxProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <Checkbox
          {...rest}
          checked={value}
          onChange={(isSelected) => onChange(isSelected)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  );
};

// -------------------- Radio group -------------------------------------

export const FormRadioGroup: React.FC<
  Omit<FormRadioGroupProps, 'onChange'>
> = ({ name, control, defaultValue, rules = {}, errors, ...rest }) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <RadioButtons
          name={name}
          value={value}
          onChange={(value) => onChange(value)}
          {...rest}
        />
      )}
    />
  );
};

// -------------------- Date picker field -------------------------------------

export const FormDatePicker: React.FC<FormDatePickerProps> = ({
  name,
  control,
  defaultValue,
  rules,
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <DatePickerField
          {...rest}
          value={value}
          onChange={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  );
};
