import * as dayjs from 'dayjs';
import { DateString } from '@rallx-artis/common-types';

// -----------------------------------------------------------------------

export const dateFmt = (value: DateString, format?: string | boolean) => {
  if (dayjs(value).isValid()) {
    const isTime = typeof format === 'boolean' && !!format;

    return dayjs(value).format(
      isTime ? 'DD MMM YYYY, hh:mm A' : (format as string) || 'DD MMM YYYY'
    );
  }
  return '';
};

// -----------------------------------------------------------------------
