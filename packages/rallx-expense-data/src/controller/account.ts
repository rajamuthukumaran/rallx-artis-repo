import { DataSource, Repository } from 'typeorm';
import { Account } from '../entities/account';
import { AccountData } from '../interface/account';
import { AuthObject, RepoData } from '../interface/common';
import { Options, genPromise } from '../utils';

export class AccountController {
  private repo: Repository<Account>;

  private conn: DataSource;

  private userInfo: AuthObject;

  constructor(args: RepoData) {
    this.repo = args.conn.getRepository(Account);
    this.conn = args.conn;
    this.userInfo = args.authInfo;
  }

  getAllAccounts = (options?: Options): Promise<Account[]> => {
    return genPromise(
      () => this.repo.find({ where: { user_id: this.userInfo.uid } }),
      options
    );
  };

  getAccount = (id: string, options?: Options): Promise<Account> => {
    return genPromise(() => this.repo.findOne({ where: { id } }), options);
  };

  getTotalAmount = async (showAllBal?: boolean): Promise<number> => {
    const { sum } = await this.repo
      .createQueryBuilder('acc')
      .select('SUM(acc.balance)', 'sum')
      .where(
        `acc.user_id = :uid${!showAllBal ? ` AND acc.include_in_bal = 1` : ''}`,
        {
          uid: this.userInfo.uid,
        }
      )
      .getRawOne();

    return sum;
  };

  addAccount = (data: AccountData, options?: Options): Promise<unknown> => {
    return genPromise(() => {
      const newAccount = this.repo.create({
        ...data,
        name: data?.name?.trim(),
        user_id: this.userInfo.uid,
      });
      return this.repo.save(newAccount);
    }, options);
  };

  updateAccount = (
    id: string,
    data: AccountData,
    options?: Options
  ): Promise<unknown> => {
    return genPromise(
      () =>
        this.repo.update(
          { id },
          {
            ...data,
            name: data?.name?.trim(),
          }
        ),
      options
    );
  };

  removeAccount = (id: string, options?: Options): Promise<unknown> => {
    return genPromise(() => this.repo.delete({ id }), options);
  };
}
