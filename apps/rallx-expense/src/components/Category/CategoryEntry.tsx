import { View } from 'react-native';
import { Category } from '@rallx-artis/rallx-expense-data';
import {
  IconButton,
  PopoverConfirmation,
  Text,
  TextField,
  useTheme,
} from '@rallx-artis/rallx-mobile-material-you';
import { useState } from 'react';
import { ReactChild } from '@rallx-artis/react-common-types';
import { TouchableRipple } from 'react-native-paper';
import { OnPressCallback } from '@rallx-artis/react-native-common-types';

type Props = {
  category: Category;
  onDelete: (data: Category) => void;
  onUpdate: (data: Category) => void;
};

const CategoryEntry: React.FC<Props> = ({ category, onDelete, onUpdate }) => {
  const { colorScheme } = useTheme();
  const [isEditing, setEditing] = useState(false);
  const [name, setName] = useState(category.name);

  const handleUpdate = () => {
    onUpdate({
      ...category,
      name,
    });
    setEditing(false);
  };

  const handleCancel = () => {
    setEditing(false);
    setName(category.name);
  };

  // EDIT MODE ----------------------------------------------------------------
  if (isEditing) {
    return (
      <Container>
        <View className="w-[65%]">
          <TextField
            value={name}
            onChangeText={(text) => setName(text)}
            style={{ backgroundColor: colorScheme.primaryContainer }}
          />
        </View>

        <View className="flex-row">
          <IconButton icon="check" onPress={handleUpdate} />
          <IconButton icon="close" onPress={handleCancel} />
        </View>
      </Container>
    );
  }

  // VIEW MODE ----------------------------------------------------------------
  return (
    <Container>
      <TouchableRipple onPress={() => setEditing(true)}>
        <Text
          className="text-lg"
          style={{ fontFamily: 'Montserrat_bold' }}
          maxLength={35}
        >
          {category.name}
        </Text>
      </TouchableRipple>

      <PopoverConfirmation onConfirm={() => onDelete(category)}>
        <IconButton icon="delete" />
      </PopoverConfirmation>
    </Container>
  );
};

// CONTAINER ----------------------------------------------------------------

const Container: React.FC<ReactChild & { onPress?: OnPressCallback }> = ({
  children,
  onPress,
}) => {
  const { colorScheme } = useTheme();

  return (
    <TouchableRipple onPress={onPress}>
      <View
        className="flex-row justify-between items-center mx-4 my-2 p-4 rounded-2xl"
        style={{
          backgroundColor: colorScheme.primaryContainer,
        }}
      >
        {children}
      </View>
    </TouchableRipple>
  );
};

export default CategoryEntry;
