import { useFonts } from 'expo-font';

export const useInter = () => {
  const fontDetails = useFonts({
    Inter: require('../../../../../assets/fonts/Inter/Inter-VariableFont_slnt,wght.ttf'),
  });

  return fontDetails;
};
