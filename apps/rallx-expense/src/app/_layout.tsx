import { enGB, registerTranslation } from 'react-native-paper-dates';
import { Text, ThemeProvider } from '@rallx-artis/rallx-mobile-material-you';
import '../../global.css';
import { Stack } from 'expo-router';
import {
  DarkTheme,
  LightTheme,
  reactNativePaperDark,
  reactNativePaperLight,
} from '@rallx-artis/rallx-purple-theme';
import { useMontserrat } from '@rallx-artis/react-native-hook';
import { useColorScheme, View } from 'react-native';
import { ThemeProvider as NavigationThemeProvider } from '@react-navigation/native';
import DataProvider from '../data';
import { SafeAreaProvider } from 'react-native-safe-area-context';

registerTranslation('en-GB', enGB);

const Layout: React.FC = () => {
  const [isMontserratFontLoaded] = useMontserrat();
  const colorScheme = useColorScheme();

  if (!isMontserratFontLoaded) {
    return (
      <View>
        <Text>Loading...</Text>
      </View>
    );
  }

  return (
    <SafeAreaProvider>
      <DataProvider>
        <NavigationThemeProvider
          value={colorScheme === 'dark' ? DarkTheme : LightTheme}
        >
          <ThemeProvider
            theme={{
              dark: reactNativePaperDark,
              light: reactNativePaperLight,
            }}
            fontFamily="Montserrat"
          >
            <Stack>
              <Stack.Screen name="(tabs)" options={{ headerShown: false }} />
              <Stack.Screen name="(models)" options={{ headerShown: false }} />
            </Stack>
          </ThemeProvider>
        </NavigationThemeProvider>
      </DataProvider>
    </SafeAreaProvider>
  );
};

export const unstable_settings = {
  initialRouteName: '(tabs)',
};

export default Layout;
