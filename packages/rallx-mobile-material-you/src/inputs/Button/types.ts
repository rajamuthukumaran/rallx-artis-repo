import type { GestureResponderEvent } from 'react-native';
import { ReactNativeComponentBase } from '@rallx-artis/react-native-common-types';

export type ButtonProps = ReactNativeComponentBase<{
  title?: string;
  onPress?: (e: GestureResponderEvent) => void;
  rightIcon?: any;
  leftIcon?: any;
  disabled?: boolean;
  loading?: boolean;
  variant?: 'transparent' | 'outlined' | 'solid' | 'floating' | 'secondary';
}>;
