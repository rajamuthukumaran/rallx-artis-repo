import React, { useContext, useEffect, useMemo, useState } from 'react';
import { ReactChild } from '@rallx-artis/react-common-types';
import { AuthObject } from '@rallx-artis/rallx-expense-data';

const Auth = React.createContext<AuthObject>({
  uid: '',
  isAuthendicated: false,
  isCheckingAuth: false,
});

const Authendication: React.FC<ReactChild> = ({
  children,
}): React.ReactElement => {
  const [authInfo, setAuthInfo] = useState<AuthObject>({
    uid: '',
    isAuthendicated: false,
    isCheckingAuth: false,
  });
  const authMemo = useMemo(() => authInfo, [authInfo]);

  useEffect(() => {
    setAuthInfo({
      uid: 'rajamuthukumaran333@gmail.com',
      name: 'Rajamuthukumaran D',
      email: 'rajamuthukumaran333@gmail.com',
      isAuthendicated: true,
      isCheckingAuth: false,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <Auth.Provider value={authMemo}>{children}</Auth.Provider>;
};

export const useAuth = () => {
  const auth = useContext(Auth);

  return auth;
};

export default Authendication;
