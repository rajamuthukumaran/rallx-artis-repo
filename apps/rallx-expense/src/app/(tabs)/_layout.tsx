// import { Tabs } from 'expo-router';
import {
  Entypo,
  FontAwesome5,
  MaterialCommunityIcons,
  MaterialIcons,
} from '@expo/vector-icons';
import { withLayoutContext } from 'expo-router';

import { createMaterialBottomTabNavigator } from 'react-native-paper/react-navigation';
import Header from '../../components/Header';
import AddExpenseButton from '../../components/Expense/AddExpenseButton';

const { Navigator } = createMaterialBottomTabNavigator();

const Tabs = withLayoutContext(Navigator);

const Layout: React.FC = () => {
  return (
    <>
      <Header />
      <Tabs>
        <Tabs.Screen
          name="index"
          options={{
            title: 'Expense',
            headerShown: false,
            tabBarIcon: ({ color }: never) => (
              <FontAwesome5 name="money-bill-wave" size={24} color={color} />
            ),
          }}
        />
        <Tabs.Screen
          name="Accounts"
          options={{
            title: 'Accounts',
            tabBarIcon: ({ color }: never) => (
              <Entypo name="wallet" size={24} color={color} />
            ),
          }}
        />
        <Tabs.Screen
          name="Budget"
          options={{
            title: 'Budget',
            tabBarIcon: ({ color }: never) => (
              <MaterialCommunityIcons
                name="clipboard-list-outline"
                size={24}
                color={color}
              />
            ),
          }}
        />
        <Tabs.Screen
          name="Settings"
          options={{
            title: 'Settings',
            tabBarIcon: ({ color }: never) => (
              <MaterialIcons name="settings" size={24} color={color} />
            ),
          }}
        />
      </Tabs>
      <AddExpenseButton />
    </>
  );
};

export default Layout;
