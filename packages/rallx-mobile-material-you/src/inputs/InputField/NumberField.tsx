import { useMemo } from 'react';
import { TextField } from './TextField';
import { InputFieldProps } from './types';
import { numberFmt } from '@rallx-artis/common-functions';

export const NumberField: React.FC<
  InputFieldProps & { isCurrency?: boolean }
> = ({ keyboardType, onChangeText, value, isCurrency, ...rest }) => {
  const formattedValue = useMemo(() => {
    const fmtValue = numberFmt(value || '', {
      activeChange: true,
    });

    if (isCurrency && value) {
      return `₹ ${fmtValue}`;
    }

    return fmtValue || '';
  }, [isCurrency, value]);

  const handleChange = (text: string) => {
    const refineValue = text?.match(/[+-]?((\d+\.?\d*)|(\.\d+))/g)?.join(''); // remove , and other formatting string
    onChangeText && onChangeText(refineValue || '');
  };

  return (
    <TextField
      keyboardType={keyboardType ?? 'numeric'}
      value={formattedValue}
      onChangeText={handleChange}
      {...rest}
    />
  );
};
