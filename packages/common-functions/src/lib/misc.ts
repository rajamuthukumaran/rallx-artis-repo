// ----------------------------------------------------------------

export const nullify = (value: any): any => {
  return Object.keys(value || {}).reduce((a, key: any) => {
    const curValue = value[key];
    return {
      ...a,
      [key]: curValue || curValue === 0 || curValue === false ? curValue : null,
    };
  }, {});
};

// -----------------------------------------------------------------------

export const textEllipsis = (text?: string, maxLength?: number) => {
  return text && maxLength !== undefined && text?.length > maxLength
    ? `${text.substring(0, maxLength - 3)}...`
    : text;
};
