import { useTheme as useThemeCommonFN } from '@rallx-artis/react-hooks';
import { useColorScheme } from 'react-native';
import { MD3DarkTheme, MD3LightTheme } from 'react-native-paper';

export const useTheme = () => {
  const theme = useThemeCommonFN();
  const deviceColorScheme = useColorScheme();
  const { themeConfig } = theme;

  const colorScheme =
    deviceColorScheme === 'dark'
      ? themeConfig.theme?.dark?.colors ?? MD3DarkTheme.colors
      : themeConfig.theme?.light?.colors ?? MD3LightTheme.colors;

  return {
    ...theme,
    colorScheme: colorScheme as typeof MD3DarkTheme.colors,
    themeConfig: {
      ...themeConfig,
      themeMode: themeConfig.themeMode || deviceColorScheme,
    },
  };
};
