/* eslint-disable @typescript-eslint/no-explicit-any */
import { logError } from '@rallx-artis/common-functions';
import type { DataSource } from 'typeorm';

export type Options<T = any> = {
  onCallback?: () => any;
  onSuccess?: (res: T) => any;
  onFail?: (err: any) => any;
  setLoading?: any;
};

export type GenPromise<T = any> = (
  repo: () => Promise<any>,
  options?: Options<T>
) => Promise<any>;

export const genPromise: GenPromise = async (repo, options) => {
  const { onSuccess, onFail, onCallback, setLoading } = options || {};

  return new Promise((resolve, reject) => {
    if (setLoading) setLoading(true);
    repo()
      .then((res) => {
        if (onSuccess) onSuccess(res);
        resolve(res);
      })
      .catch((err) => {
        logError(err);
        if (onFail) onFail(err);
        reject(err);
      })
      .finally(() => {
        if (onCallback) onCallback();
        if (setLoading) setLoading(false);
      });
  });
};

export const insertTypeObject = (
  value: any,
  conn: DataSource,
  entity: any,
  relations?: string[]
) => {
  if (!!value && typeof value === 'string') {
    return conn
      .getRepository(entity)
      .findOne({ where: { id: value }, relations });
  }
  return value;
};
