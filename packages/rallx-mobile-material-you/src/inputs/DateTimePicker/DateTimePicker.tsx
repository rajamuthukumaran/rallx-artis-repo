import { View } from 'react-native';
import { DatePickerField } from './DatePicker';
import { TimePickerField } from './TimePicker';
import React, { useEffect, useState } from 'react';
import { DateTimePickerProps, TimePickerObject } from './types';
import { ErrorText, Text } from '../../outputs';
import dayjs from 'dayjs';
import clsx from 'clsx';

export const DateTimePicker: React.FC<DateTimePickerProps> = ({
  value,
  label,
  dateLable,
  timeLable,
  onChange,
  error,
  validDates,
  unmountErrorWhenNoErrors,
  fieldContainerClassName,
  className,
  ...rest
}) => {
  const [time, setTime] = useState<TimePickerObject>({
    hours: value ? dayjs(value).hour() : undefined,
    minutes: value ? dayjs(value).minute() : undefined,
  });
  const [date, setDate] = useState<Date | undefined>(value);

  useEffect(() => {
    if (onChange) {
      const formattedDate = dayjs(date || new Date())
        .hour(time?.hours || 0)
        .minute(time?.minutes || 0);

      onChange(formattedDate.toDate());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [time, date]);

  return (
    <View className={className}>
      {label && <Text>{label}</Text>}
      <View
        className={clsx(
          'flex-row gap-6 justify-between',
          fieldContainerClassName
        )}
      >
        <DatePickerField
          className="flex-1"
          label={dateLable}
          value={date}
          onChange={(date) => setDate(date)}
          validDates={validDates}
          hasError={!!error}
          {...rest}
        />
        <TimePickerField
          className="flex-1"
          label={timeLable}
          value={time}
          onChange={(time) => setTime(time)}
          hasError={!!error}
          {...rest}
        />
      </View>
      <ErrorText
        unmountErrorWhenNoErrors={unmountErrorWhenNoErrors}
        error={error}
      />
    </View>
  );
};
