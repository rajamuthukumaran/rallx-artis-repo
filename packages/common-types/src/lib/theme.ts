export type ColorScheme = {
  '50'?: string;
  '100'?: string;
  '200'?: string;
  '300'?: string;
  '400'?: string;
  '500'?: string;
  '600'?: string;
  '700'?: string;
  '800'?: string;
  '900'?: string;
};

export type ThemeMode = 'dark' | 'light';

export type Fonts = {
  normal200: string;
  normal400: string;
  normal500: string;
  normal600: string;
  normal700: string;
  normal900: string;
  Italic400: string;
  Italic600: string;
  Italic700: string;
};
