// ----------------------------------------------------------------

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const logError = async (message: any) => {
  //   const shouldLog = !!(await getAsyncData('shouldLogErrors'))

  if (__DEV__) {
    console.error(message)
  }
  //   else if (shouldLog && !__DEV__) {
  //     await setAsyncData('errorLogs', [JSON.stringify(message)], true)
  //   }
}

// -----------------------------------------------------------------------
