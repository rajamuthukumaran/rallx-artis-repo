import { Link } from 'expo-router';
import { FAB } from '@rallx-artis/rallx-mobile-material-you';
import { View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const AddExpenseButton: React.FC = () => {
  const { bottom } = useSafeAreaInsets();

  return (
    <View
      className="absolute m-8 right-0"
      style={{
        bottom: bottom + 100,
      }}
    >
      <Link href="AddExpense">
        <FAB variant="secondary" icon="plus" />
      </Link>
    </View>
  );
};

export default AddExpenseButton;
