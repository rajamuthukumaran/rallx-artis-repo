import { TextInput } from 'react-native-paper';
import { InputFieldProps } from './types';
import { View } from 'react-native';
import { ErrorText } from '../../outputs';

export const TextField: React.FC<InputFieldProps> = ({
  error,
  loading,
  disabled,
  leftIcon,
  rightIcon,
  maxLength,
  value,
  showLength,
  unmountErrorWhenNoErrors,
  ...rest
}) => {
  return (
    <View>
      <TextInput
        value={value}
        disabled={disabled || loading}
        error={!!error}
        right={
          rightIcon ? (
            <TextInput.Icon icon={rightIcon} />
          ) : (
            showLength &&
            maxLength && (
              <TextInput.Affix text={`${value?.length || 0}/${maxLength}`} />
            )
          )
        }
        left={leftIcon && <TextInput.Icon icon={leftIcon} />}
        maxLength={maxLength}
        {...rest}
      />
      <ErrorText
        unmountErrorWhenNoErrors={unmountErrorWhenNoErrors}
        error={error}
      />
    </View>
  );
};
