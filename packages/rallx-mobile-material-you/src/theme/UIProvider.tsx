import React from 'react';
import {
  MD3DarkTheme,
  MD3LightTheme,
  PaperProvider,
  configureFonts,
} from 'react-native-paper';
import { ReactChild } from '@rallx-artis/react-common-types';
import { useTheme } from './useTheme';

export const UIProvider: React.FC<ReactChild> = ({ children }) => {
  const { themeConfig, colorScheme } = useTheme();
  const { fontFamily } = themeConfig || {};
  const isDark = themeConfig?.themeMode === 'dark';
  const defaultTheme = isDark ? MD3DarkTheme : MD3LightTheme;

  return (
    <PaperProvider
      theme={{
        ...defaultTheme,
        dark: isDark,
        colors: colorScheme,
        fonts: fontFamily
          ? configureFonts({
              config: {
                fontFamily: fontFamily,
              },
            })
          : defaultTheme.fonts,
      }}
    >
      {children}
    </PaperProvider>
  );
};

export default UIProvider;
