import {
  FAB,
  TextField,
  Toaster,
  useTheme,
  useToaster,
} from '@rallx-artis/rallx-mobile-material-you';
import { ScrollView, View } from 'react-native';
import { useDatabase } from '../../data';
import {
  DeleteError,
  Category as ICategory,
} from '@rallx-artis/rallx-expense-data';
import { useState } from 'react';
import { useMountEffect } from '@rallx-artis/react-native-hook';
import CategoryEntry from '../../components/Category/CategoryEntry';

const AddExpense: React.FC = () => {
  const {
    colorScheme,
    themeConfig: { themeMode },
  } = useTheme();
  const database = useDatabase();
  const { toasterConfig, setToaster } = useToaster();

  const [categories, setCategories] = useState<ICategory[]>([]);
  const [isLoading, setLoading] = useState(false);

  const [newCategory, setNewCategory] = useState('');

  // -------------------- Database ops -------------------------------------

  const getCategories = () => {
    database.categoryRepo
      .getAllCategories({ setLoading })
      .then((res) => setCategories(res));
  };

  const addCategory = () => {
    if (newCategory) {
      if (!categories?.find((f) => f.name === newCategory)) {
        const req = {
          name: newCategory,
        };

        database.categoryRepo.addCategories(req, { setLoading }).then(() => {
          getCategories();
          setNewCategory('');
        });
      } else {
        setToaster({
          message: 'This category is already in use, so this cannot be deleted',
          variant: 'error',
        });
      }
    }
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const updateCategory = (data: ICategory) => {
    database.categoryRepo.addCategories(data, { setLoading }).then(() => {
      getCategories();
    });
  };

  const deleteCategory = (data: ICategory) => {
    database.categoryRepo.removeCategory(data, { setLoading }).then((res) => {
      getCategories();

      if (res?.internalError || res?.internalError === 0) {
        const errCode = res?.internalError;

        if (errCode === DeleteError.EXP_EXIST) {
          setToaster({
            message:
              'This category is already in use, so this cannot be deleted',
            variant: 'error',
          });
        }
      }
    });
  };

  // ------------------------- Events -------------------------------------

  useMountEffect(() => {
    getCategories();
  }, []);

  // -----------------------------------------------------------------------

  return (
    <View
      className="flex-col justify-between h-full pt-3"
      style={{ backgroundColor: colorScheme.background }}
    >
      <ScrollView>
        {categories.map((category) => (
          <CategoryEntry
            key={category.id}
            category={category}
            onDelete={deleteCategory}
            onUpdate={updateCategory}
          />
        ))}
      </ScrollView>

      <View
        className="p-6 rounded-t-xl flex-row gap-6 items-center justify-between"
        style={{
          backgroundColor:
            themeMode === 'dark' ? colorScheme.backdrop : colorScheme.primary,
        }}
      >
        <View className="flex-1">
          <TextField
            value={newCategory}
            onChangeText={(text) => setNewCategory(text)}
            loading={isLoading}
            placeholder="Enter new Category"
            style={{
              backgroundColor: colorScheme.background,
            }}
          />
        </View>
        <FAB
          disabled={!newCategory}
          loading={isLoading}
          icon="plus"
          variant="primary"
          onPress={addCategory}
        />
      </View>
      <Toaster {...toasterConfig} />
    </View>
  );
};

export default AddExpense;
