import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Account } from './account';
import { Category } from './category';

@Entity('loan')
export class Loan {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column()
  user_id!: string;

  @Column()
  name!: string;

  @Column()
  type!: string;

  @ManyToOne(() => Category, { nullable: true })
  @JoinColumn()
  category!: Category;

  @ManyToOne(() => Account, { nullable: true })
  @JoinColumn()
  account!: Account;

  @Column()
  total_amount!: number;

  @Column({ nullable: true })
  balance!: number;

  @Column({ nullable: true })
  payment!: number;

  @Column({ nullable: true })
  opened_date!: Date;

  @Column({ nullable: true })
  due_date!: number;

  @Column({ nullable: true })
  exp_date!: Date;

  @Column({ nullable: true })
  interest!: number;

  @Column({ nullable: true })
  default_reward_point!: number;

  @Column({ nullable: true })
  round_off_reward!: boolean;

  @Column({ nullable: true })
  fees!: number;

  @Column()
  is_reloadable!: boolean;

  @Column()
  is_autopay!: boolean;

  @Column({ nullable: true })
  description!: string;
}
