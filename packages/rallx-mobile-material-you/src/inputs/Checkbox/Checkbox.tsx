import { View } from 'react-native';
import { Checkbox as RNCheckbox } from 'react-native-paper';
import { CheckBoxProps } from './types';
import { ErrorText, Text } from '../../outputs';
import { useTheme } from '../../theme';

export const Checkbox: React.FC<CheckBoxProps> = ({
  label,
  checked,
  loading,
  disabled,
  color,
  error,
  onChange,
  ...rest
}) => {
  const { colorScheme } = useTheme();

  return (
    <View>
      <View className="flex-row items-center">
        <RNCheckbox
          status={checked ? 'checked' : 'unchecked'}
          disabled={loading || disabled}
          color={error ? colorScheme.error : color}
          onPress={() => onChange && onChange(!checked)}
          {...rest}
        />
        {label && <Text>{label}</Text>}
      </View>
      <ErrorText error={error} />
    </View>
  );
};
