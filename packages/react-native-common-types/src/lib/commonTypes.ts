import type { ForwardedRef } from 'react';
import { ReactChild } from '@rallx-artis/react-common-types';
import { StyleProp } from 'react-native';

export type ReactNativeComponentBase<C = any, T = any, R = any> = C & {
  ref?: ForwardedRef<R>;
  style?: StyleProp<T>;
  className?: string;
  testID?: string;
} & ReactChild;
